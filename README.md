# Torrents On Boats - BitTorrent search engine

<p align="center"><a href="https://github.com/Looney-Dev/TorrentsOnBoats"><img src="https://raw.githubusercontent.com/Looney-Dev/TorrentsOnBoats/master/resources/Torrents On Boats Banner.png"></a></p>

[![CircleCI Build Status](https://circleci.com/gh/Looney-Dev/TorrentsOnBoats.png?style=shield)](https://circleci.com/gh/Looney-Dev/TorrentsOnBoats)
[![Appveyor Build Status](https://ci.appveyor.com/api/projects/status/1eh0lug97fboscib?svg=true)](https://ci.appveyor.com/project/Looney-Dev/TorrentsOnBoats)
[![Travis Build Status](https://travis-ci.org/Looney-Dev/TorrentsOnBoats.svg?branch=master)](https://travis-ci.org/Looney-Dev/TorrentsOnBoats)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![Release](https://img.shields.io/github/release/Looney-Dev/TorrentsOnBoats.svg)](https://github.com/Looney-Dev/TorrentsOnBoats/releases)
[![Documentation](https://img.shields.io/badge/docs-faq-brightgreen.svg)](https://github.com/Looney-Dev/TorrentsOnBoats/blob/master/docs/MANUAL.md) [![Greenkeeper badge](https://badges.greenkeeper.io/Looney-Dev/TorrentsOnBoats.svg)](https://greenkeeper.io/)

BitTorrent search program for desktop. Collect and navigate over base of torrents statistic, categories, and give easy access to it. Work over p2p network and support Windows, Linux, Mac OS platforms.

## Features
* Works over p2p torrent network, doesn't require any trackers
* Supports own p2p protocol for additional data transfer (like search between clients, descriptions/votes transfer, etc.)
* Search over torrent collection
* Torrent and files search
* Search filters (size ranges, files, seeders etc...)
* Collection filters (regex filters, adult filters)
* Trackers peers scan supported
* Integrated torrent client
* Collect only statistic information and don't save any torrents internal data
* Supports torrents rating (voting)
* P2P Search protocol.
* ~~Web version (web interface) for servers~~
* Top list (mostly common and popular torrents)
* Feed list (Rats clients activity feed)
* Translations: English, Russian, Ukrainian, Chinese
* Drag and drop torrents (expand local search database with specific torrents)
* Descriptions association from trackers

## License
[MIT](https://github.com/Looney-Dev/TorrentsOnBoats/blob/master/LICENSE)

## TODO

Web client.
