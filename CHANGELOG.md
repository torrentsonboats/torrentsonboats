## [1.1.0] New Release

This software was a private until we figured out how to keep all the torrent in database without overloading our servers during the tests conducted. Hence we're glad to release our new API system to prevent to overload our servers and keep the torrent updates flowing. 

# [BUG FIX]

There was a bug regarding where the torrent files never loaded properly nor the speed was expected. This issue was fixed.

# [NEW FEATURES]

Added [1337x.to](https://1337x.to) crawler to crawl results from 1337x.to

# [NOTE]

**1.** We will henceforth record all the bug reports via GitHub Issue Tracker. [Click here](https://github.com/Looney-Dev/TorrentOnBoats/issues) to visit our issue tracker.

**2.** Looking for Contributors. Please email me at [admin@looney.tech](mailto:admin@looney.tech).
 