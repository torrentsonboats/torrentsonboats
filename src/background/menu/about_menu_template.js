import { app, BrowserWindow, shell } from "electron";
import path from "path";
import url from "url";
import __ from '../../app/translation'

export const aboutMenuTemplateFunc = () => ({
	label: __("About"),
	submenu: [
		{
			label: __("Changelog"),
			accelerator: "CmdOrCtrl+]",
			click: () => {
				const win = new BrowserWindow({
					parent: BrowserWindow.getFocusedWindow(),
					modal: true,
					webPreferences: {
						nodeIntegration: true
					}
				})
				win.setMenu(null)
				win.loadURL(url.format({
					pathname: path.join(__dirname, "app.html"),
					protocol: "file:",
					slashes: true
				}))
				win.webContents.on('did-finish-load', () => {
					setTimeout(() => win.send('url', '/changelog'), 0)
				});

				const handleRedirect = (e, url) => {
					if(url != win.webContents.getURL()) {
						e.preventDefault()
						shell.openExternal(url)
					}
				}
        
				win.webContents.on('will-navigate', handleRedirect)
				win.webContents.on('new-window', handleRedirect)
			},
		},
		{
			label: __("Bug Report"),
			accelerator: "CmdOrCtrl+[",
			click: () => {
				shell.openExternal('https://github.com/Looney-Dev/TorrentsOnBoats/issues')
			},
		},
		{
			label: __("Support This Project"),
			accelerator: "CmdOrCtrl+*",
			click: () => {
				shell.openExternal('https://donate.torrentsonboats.gq')
			},
		},
		{
			label: __("Help (Documentation)"),
			accelerator: "CmdOrCtrl+?",
			click: () => {
				shell.openExternal('')
			},
		},
		{
			label: __("Support (Discussion)"),
			accelerator: "CmdOrCtrl+>",
			click: () => {
				shell.openExternal('')
			},
		},
		{
			label: __("About"),
			accelerator: "CmdOrCtrl+<",
			click: () => {
				shell.openExternal('https://torrentsonboats.gq')
			},
		}
	]
});
